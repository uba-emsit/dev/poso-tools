﻿using System;
using System.Collections;
using System.IO;
using System.Security;
using System.Text;
using M4DBO;
using PoSo;


namespace PoSoKMLConverter 
{
    class KMLConverter
    {
        private static String PROGRAM_NAME = "PoSo KML Converter";
        private static String RESULT_FILE = "C:\\Users\\hausmann\\Dropbox\\pointSource.kml";

        private static Database poso = new Database();
        private static String COLLECTIVE_ID = "PoSo_KML_Converter";
        //private static String COLLECTIVE_ID = "Schmutzfinken";

        private static DateTime YEAR = new DateTime(2010, 1, 1);

        private static TextWriter writer;
        
        static void Main(string[] args)
        {
            poso.Connect(PROGRAM_NAME);

            // Open result file writer
            writer = new StreamWriter(RESULT_FILE, false, Encoding.UTF8);

            Console.WriteLine("This is the PoSo KML Converter!");
            Console.WriteLine("Press <Esc> to cancel, or any other key to continue!");
            if (Console.ReadKey().Key.Equals(ConsoleKey.Escape)) return;

            Start();

            // Close and flush result file
            Console.WriteLine("Flushing results to file: " + RESULT_FILE);
            writer.Close();
            poso.Disconnect();

            Console.WriteLine("Finished! Press any key to exit.");
            Console.ReadKey();
        }

        private static void Start()
        {
            Console.WriteLine("Okay, I go start my work now...");

            WritePreamble();
            
            dboList pointSources = Program.CreatePointSourceList(poso, COLLECTIVE_ID);

            for (int count = 1; count <= pointSources.Count; count++)
            {
                PointSource pointSource = new PointSource(poso.db, pointSources[count].ToString());

                Console.WriteLine("(" + count + "/" + pointSources.Count + ") " +
                    "Processing point source \"" + pointSource.GetName() + "\"");
                WritePointSource(pointSource);
            }
            
            WriteFinish();
        }

        private static void WritePreamble()
        {
            writer.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            writer.WriteLine("<kml xmlns=\"http://www.opengis.net/kml/2.2\">");
            writer.WriteLine("<Document>");
            writer.WriteLine("<Style id=\"mouseOver\"><LabelStyle><scale>1.0</scale></LabelStyle></Style>");
            writer.WriteLine("<Style id=\"normal\"><LabelStyle><scale>0</scale></LabelStyle></Style>");
            writer.WriteLine("<StyleMap id=\"styleMap\"><Pair><key>normal</key><styleUrl>#normal</styleUrl></Pair>");
            writer.WriteLine("<Pair><key>highlight</key><styleUrl>#mouseOver</styleUrl></Pair></StyleMap>");
        }

        private static void WritePointSource(PointSource ps)
        {
            String coordinates = ps.GetCoordinates();
            
            // Check whether any coordinates can be found
            if (coordinates == null)
            {
                Console.WriteLine("No coordinates for \"" + ps.GetName() + "\" in \"" + ps.GetLocation() + "\"! Skipped...");
                return;
            }
            
            // If outside Germany use geo coding
            if (!GeoHelper.IsInsideGermany(coordinates))
            {
                Console.WriteLine("Falling back to geo coding for \"" + ps.GetName() + "\" in \"" + ps.GetLocation() + "\"...");
                coordinates = ps.GetCoordinatesFromGeoLocation();
            }
            
            // Check whether these coordinates are finally good
            if (coordinates != null && GeoHelper.IsInsideGermany(coordinates))
                WriteMainPart(ps, coordinates);
            else Console.WriteLine("No or outside Germany coordinates for \"" + ps.GetName() + "\" in \"" + ps.GetLocation() + "\"! Skipped...");
        }

        private static void WriteMainPart(PointSource ps, String coordinates)
        {
            String location = ps.GetLocation() + ", " + ps.GetState().LongName();

            writer.WriteLine("<Placemark>");
            writer.WriteLine("<styleUrl>#styleMap</styleUrl>");
            writer.WriteLine("<Style><IconStyle><scale>");
            writer.WriteLine(CalculateIconScaleValue(ps.GetThermalOutput(YEAR), 100, 1500));
            writer.WriteLine("</scale>");
            writer.WriteLine("<Icon><href>pointSource.png</href></Icon></IconStyle></Style>");
            writer.WriteLine("<Point>");
            writer.WriteLine("<coordinates>" + coordinates + "</coordinates>");
            writer.WriteLine("</Point>");
            writer.WriteLine("<name>" + SecurityElement.Escape(ps.GetName()) + " (" + location + ")</name>");
            writer.WriteLine("<Snippet>");
            writer.WriteLine(location);
            writer.WriteLine("FWL: " + ps.GetThermalOutput(YEAR) + " MW");
            writer.WriteLine("</Snippet>");

            WriteDescription(ps);

            writer.WriteLine("</Placemark>");
        }

        private static void WriteDescription(PointSource ps)
        {
            writer.WriteLine("<description>");
            
            writer.WriteLine("<style type=\"text/css\">");
            writer.WriteLine("th {background: #E8D9E0; border: solid 1px black;}");
            writer.WriteLine("th.left {text-align: left;}");
            writer.WriteLine("td {background: #D9EDEE; border: solid 1px black; text-align: right;}");
            writer.WriteLine("</style>");
    
            writer.WriteLine("<div style=\"border: 1px solid black; padding: 1em; width: 500px;\">");
			writer.WriteLine("<div style=\"float: left; width: 50%\">");
			writer.WriteLine("<h4>Allgemeine Informationen</h4>");
            writer.WriteLine("<ul><li>Betreiber: " + GetOperatorName(ps) + "</li>");
            //writer.WriteLine("<li>Sektor: ???</li>");
			writer.WriteLine("<li>Feuerungswärmeleistung: " + ps.GetThermalOutput(YEAR) + " MW</li></ul>");

            if (ps.GetPRTRId() != -1 || ps.GetETSId() != -1)
            {
                writer.WriteLine("Weitere Informationen: ");
                if (ps.GetPRTRId() != -1) 
                    writer.WriteLine("<a href=\"http://prtr.bund.de/x_prtr_suche/index.php?betrieb_id=" + ps.GetPRTRId() + "\">PRTR</a>");
                if (ps.GetPRTRId() != -1 && ps.GetETSId() != -1) writer.WriteLine(", ");
                if (ps.GetETSId() != -1) 
                    writer.WriteLine("<a href=\"https://www.register.dehst.de/crweb/report/public/installationDetail.do?accountId=" + ps.GetETSId() + "\">ETS</a>");
            }
            writer.WriteLine("</div>");

			writer.Write("<img style=\"float: right; width: 45%;\" ");

            if (ps.GetPictureURL() != null)
                writer.WriteLine("src=\"" + ps.GetPictureURL() + "\"/>");
            else writer.WriteLine("src=\"http://www.lauritzen-hamburg.de/fotosekke/fabrik_m_johler.jpg\" />");

			//writer.WriteLine("<h4 style=\"clear: both;\">Brennstoffeinsätze</h4>");

            writer.WriteLine("<h4 style=\"clear: both;\">Emissionen</h4>");
            double[] emSOx = ps.GetSulphurOxideEmission(YEAR.AddYears(-5), YEAR);
            double[] emNOx = ps.GetNitrogenOxideEmission(YEAR.AddYears(-5), YEAR);
            double[] emTsp = ps.GetTSPEmission(YEAR.AddYears(-5), YEAR);
            
            writer.WriteLine("<table>");
            writer.WriteLine("<tr><th>Alle Werte in Tonnen</th>");
            writer.WriteLine("<th>" + YEAR.AddYears(-5).Year + "</th><th>" + YEAR.AddYears(-4).Year + "</th>");
            writer.WriteLine("<th>" + YEAR.AddYears(-3).Year + "</th><th>" + YEAR.AddYears(-2).Year + "</th>");
            writer.WriteLine("<th>" + YEAR.AddYears(-1).Year + "</th><th>" + YEAR.Year + "</th></tr>");
            //writer.WriteLine("<th>Trend</th><th>Rang</th></tr>");
            writer.WriteLine("<tr><th class=\"left\">Schwefeloxide (SOx)</th>");
            writer.WriteLine("<td>" + emSOx[0] + "</td><td>" + emSOx[1] + "</td>");
            writer.WriteLine("<td>" + emSOx[2] + "</td><td>" + emSOx[3] + "</td>");
            writer.WriteLine("<td>" + emSOx[4] + "</td><td>" + emSOx[5] + "</td></tr>");
            writer.WriteLine("<tr><th class=\"left\">Stickoxide (NOx)</th>");
            writer.WriteLine("<td>" + emNOx[0] + "</td><td>" + emNOx[1] + "</td>");
            writer.WriteLine("<td>" + emNOx[2] + "</td><td>" + emNOx[3] + "</td>");
            writer.WriteLine("<td>" + emNOx[4] + "</td><td>" + emNOx[5] + "</td></tr>");
            writer.WriteLine("<tr><th class=\"left\">Staub (TSP)</th>");
            writer.WriteLine("<td>" + emTsp[0] + "</td><td>" + emTsp[1] + "</td>");
            writer.WriteLine("<td>" + emTsp[2] + "</td><td>" + emTsp[3] + "</td>");
            writer.WriteLine("<td>" + emTsp[4] + "</td><td>" + emTsp[5] + "</td></tr>");
            writer.WriteLine("</table>");

            writer.WriteLine("</div>");
            writer.WriteLine("</description>");
        }

        private static void WriteFinish()
        {
            writer.WriteLine("</Document></kml>");
        }

        private static string CalculateIconScaleValue(double value, double lowerLimit, double upperLimit)
        {
            if (value < lowerLimit) return "0.5";
            else if (value > upperLimit) return "1.3";
            else return (0.5 + 0.8 * (value / upperLimit) + "").Replace(",", ".").Substring(0, 3);
        }

        private static string GetOperatorName(PointSource ps)
        {
            if (ps.GetOperator() == null || ps.GetOperator().Length < 2) return "<i>keine Information</i>";
            else return SecurityElement.Escape(ps.GetOperator());
        }
    }
}
