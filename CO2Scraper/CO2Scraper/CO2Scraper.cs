﻿using System;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using M4DBO;
using PoSo;

namespace PoSoCO2Scraper
{
    class CO2Scraper
    {
        private static String PROGRAM_NAME = "PoSo CO2 Scraper";
        private static String RESULT_FILE = "C:\\Dokumente und Einstellungen\\hausmann\\Desktop\\My Dropbox\\co2emissions.csv";

        private static Database poso = new Database();
        private static String COLLECTIVE_ID = "PoSo_CO2_Scraper";
        private static HtmlWeb htmlWeb = new HtmlWeb();
            
        private static DateTime START_YEAR = new DateTime(2005, 1, 1);

        private static TextWriter writer;

        static void Main(string[] args)
        {
            poso.Connect(PROGRAM_NAME);

            // Open result file writer
            writer = new StreamWriter(RESULT_FILE, false, Encoding.Default);

            Console.WriteLine("This is the PoSo CO2 Scraper!");
            Console.WriteLine("Press <Esc> to cancel, or any other key to continue!");
            if (Console.ReadKey().Key.Equals(ConsoleKey.Escape)) return;

            Start();
            
            // Flush and close
            Console.WriteLine("Flushing results to file: " + RESULT_FILE);
            writer.Close();
            poso.Disconnect();

            Console.WriteLine("Finished! Press any key...");
            Console.ReadKey();
        }

        private static void Start()
        {
            Console.WriteLine("Okay, I go start my work now...");

            WritePreamble();

            dboList pointSources = Program.CreatePointSourceList(poso, COLLECTIVE_ID);

            for (int count = 1; count <= pointSources.Count; count++)
            {
                PointSource pointSource = new PointSource(poso.db, pointSources[count].ToString());

                Console.WriteLine("(" + count + "/" + pointSources.Count + ") " +
                    "Processing point source \"" + pointSource.GetName() + "\"");
                WritePointSource(pointSource);
            }
        }
        
        private static void WritePreamble()
        {
            writer.WriteLine("Anlage;Raumbezug;Wertetyp;Schadstoff;Einheit;2005;2006;2007;2008");
        }

        private static void WritePointSource(PointSource ps)
        {
            String registerURL = ps.GetETSRegisterURL();
            if (registerURL == null) return;
            
            writer.Write(ps.GetName());
            writer.Write(";");
            writer.Write(ps.GetLocation());
            writer.Write(";");
            writer.Write("EM");
            writer.Write(";");
            writer.Write("CO2");
            writer.Write(";");
            writer.Write("t");
            writer.Write(";");
            
            HtmlDocument doc = htmlWeb.Load(registerURL);

            HtmlNode table = doc.DocumentNode.SelectSingleNode("//table[@class=\"report\"]");
            int year = START_YEAR.Year;
            long emission = GetEmissionValue(table, year);
            while (emission > 0)
            {
                writer.Write(emission + ";");
                emission = GetEmissionValue(table, ++year);
            }
            writer.WriteLine("");
        }

        private static long GetEmissionValue(HtmlNode table, int year)
        {
            long result;
            string cellValue = table.SelectSingleNode("tr[" + (year - 2003) + "]").SelectSingleNode("td[6]").InnerHtml;
            
            if (long.TryParse(cellValue.Replace(",", ""), out result)) return result;
            else return 0;
        }
    }
}
