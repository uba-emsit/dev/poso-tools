﻿using System;
using System.IO;
using System.Net;
using System.Text;
using SharpMap.CoordinateSystems;
using SharpMap.CoordinateSystems.Transformations;

namespace PoSo
{
    /// <summary>
    /// Contains some helpers for the geographic conversion process
    /// </summary>
    public class GeoHelper
    {
        /// <summary>
        /// Use Google geo coding service to locate a place by its name.
        /// </summary>
        /// <param name="place">The name of the place. E.g.: "New York".</param>
        /// <returns>The best match geo location of the place given as coordinates [long],[lat]
        ///     separated by comma. E.g.: "3.4923454,45.2321311". Returns [null] for no match.</returns>
        internal static String GeoLocate(String location)
        {
            // used to build entire input
            StringBuilder sb = new StringBuilder();

            // used on each read operation
            byte[] buf = new byte[8192];

            // prepare the web page we will be asking for
            HttpWebRequest request = (HttpWebRequest)
                WebRequest.Create("http://maps.google.com/maps/geo?q=" + location + "&output=csv");

            // execute the request
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            // we will read data via the response stream
            Stream resStream = response.GetResponseStream();

            string tempString = null;
            int count = 0;

            do
            {
                // fill the buffer with data
                count = resStream.Read(buf, 0, buf.Length);

                // make sure we read some data
                if (count != 0)
                {
                    // translate from bytes to ASCII teeastt
                    tempString = Encoding.ASCII.GetString(buf, 0, count);

                    // continue building the string
                    sb.Append(tempString);
                }
            }
            while (count > 0); // any more data to read?

            // tokenize and assemble result
            String[] token = sb.ToString().Split(',');
            String resultString = null;

            if (token[3] != "0" && token[2] != "0") resultString = token[3] + "," + token[2];

            return resultString;
        }

        /// <summary>
        /// Will roughly check whether given coordinates are located inside Germany's borders.
        /// </summary>
        /// <param name="coordinates">The coordinates to check as latitude,longitude. 
        /// E.g.: "3.4923454,45.2321311".</param>
        /// <returns>True when (roughly) inside Germany</returns>
        public static bool IsInsideGermany(string coordinates)
        {
            String[] token = coordinates.Split(',');
            double latitude = Double.Parse(token[1].Replace('.', ','));
            double longitude = Double.Parse(token[0].Replace('.', ','));

            return longitude > 5.8 && longitude < 15 && latitude > 47 && latitude < 55;
        }
    }
}
