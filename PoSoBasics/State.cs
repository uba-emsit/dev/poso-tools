﻿using System;

namespace PoSo
{
    /// <summary>
    /// Enum wraps a German "Bundesland"
    /// </summary>
    public enum State {SH=1, HH=2, NI=3, HB=4, NW=5, HE=6, RP=7, BW=8, BY=9, SL=10, BE=11, BB=12, MV=13, SN=14, ST=15, TH=16};

    public static class StateExtension
    {
        public static string LongName(this State state)
        {
            switch (state)
            {
                case State.SH: return "Schleswig-Holstein";
                case State.HH: return "Hamburg";
                case State.NI: return "Niedersachsen";
                case State.HB: return "Bremen";
                case State.NW: return "Nordrhein-Westfalen";
                case State.HE: return "Hessen";
                case State.RP: return "Rheinland-Pfalz";
                case State.BW: return "Baden-Württemberg";
                case State.BY: return "Bayern";
                case State.SL: return "Saarland";
                case State.BE: return "Berlin";
                case State.BB: return "Brandenburg";
                case State.MV: return "Mecklenburg-Vorpommern";
                case State.SN: return "Sachsen";
                case State.ST: return "Sachsen-Anhalt";
                case State.TH: return "Thüringen";
                default: throw new ArgumentOutOfRangeException("state", "No such state");
            }
        }
    }
}
