﻿using System;
using SharpMap.CoordinateSystems;
using SharpMap.CoordinateSystems.Transformations;

namespace PoSo
{
    /// <summary>
    /// Basic abstract projection wrapper. Offers a transformation to the WGS 84 "default" projection.
    /// </summary>
    public abstract class Projection 
    {
        // This is what Google Earth needs
        protected static ICoordinateSystem csWgs84Sphere = SharpMap.Converters.WellKnownText.CoordinateSystemWktReader.Parse(
            "GEOGCS[\"GCS_WGS_1984\",DATUM[\"D_WGS_1984\"," +
            "SPHEROID[\"WGS_1984\",6378137,298.257223563]]," +
            "PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.0174532925199433]]") as ICoordinateSystem;

        protected CoordinateTransformationFactory factory = new CoordinateTransformationFactory();

        /// <summary>
        /// Translates east/north values to degrees [long],[lat].
        /// </summary>
        /// <param name="east">The east value ("Rechtswert")</param>
        /// <param name="north">The north value ("Hochwert")</param>
        /// <returns>Translated coordinates from east/north values to degrees.
        ///     E.g.: "3.4923454,45.2321311". Returns [null] for invalid data.</returns>
        public abstract string getCoordinatesAsWGS84(int east, int north);

        protected string Transform(int east, int north, ICoordinateTransformation transformation)
        {
            double[] input = new double[] { east, north };
            double[] result;

            result = transformation.MathTransform.Transform(input);

            return result[0].ToString().Replace(",", ".") + "," + result[1].ToString().Replace(",", ".");
        }
    }

    public abstract class DhdnGKProjection : Projection
    {
        protected ICoordinateTransformation transformation;

        public override string getCoordinatesAsWGS84(int east, int north)
        {
            return Transform(east, north, transformation);
        }
    }

    public class DhdnGkZone2Projection : DhdnGKProjection
    {
        // DHDN / 3-degree Gauss zone 2
        private static ICoordinateSystem csDhdnZone2 = SharpMap.Converters.WellKnownText.CoordinateSystemWktReader.Parse(
            "PROJCS[\"DHDN / 3-degree Gauss zone 2\"," +
                "GEOGCS[\"DHDN\",DATUM[\"Deutsches_Hauptdreiecksnetz\"," +
                    "SPHEROID[\"Bessel 1841\",6377397.155,299.1528128," +
                        "AUTHORITY[\"EPSG\",\"7004\"]],AUTHORITY[\"EPSG\",\"6314\"]]," +
                        "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]]," +
                        "UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4314\"]]," +
                        "PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",6],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",2500000],PARAMETER[\"false_northing\",0]," +
                        "UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AUTHORITY[\"EPSG\",\"31462\"]]") as ICoordinateSystem;

        public DhdnGkZone2Projection()
        {
            transformation = this.factory.CreateFromCoordinateSystems(csDhdnZone2, csWgs84Sphere);
        }        
    }

    public class DhdnGkZone3Projection : DhdnGKProjection
    {
        // DHDN / 3-degree Gauss zone 3
        private static ICoordinateSystem csDhdnZone3 = SharpMap.Converters.WellKnownText.CoordinateSystemWktReader.Parse(
                "PROJCS[\"DHDN / 3-degree Gauss zone 3\"," +
                    "GEOGCS[\"DHDN\",DATUM[\"Deutsches_Hauptdreiecksnetz\"," +
                    "SPHEROID[\"Bessel 1841\",6377397.155,299.1528128," +
                        "AUTHORITY[\"EPSG\",\"7004\"]],AUTHORITY[\"EPSG\",\"6314\"]]," +
                        "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]]," +
                        "UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4314\"]]," +
                        "PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",9],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",3500000],PARAMETER[\"false_northing\",0]," +
                        "UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AUTHORITY[\"EPSG\",\"31463\"]]") as ICoordinateSystem;
        
        public DhdnGkZone3Projection()
        {
            transformation = this.factory.CreateFromCoordinateSystems(csDhdnZone3, csWgs84Sphere);
        }  
    }

    public class DhdnGkZone4Projection : DhdnGKProjection
    {
        // DHDN / 3-degree Gauss zone 4
        private static ICoordinateSystem csDhdnZone4 = SharpMap.Converters.WellKnownText.CoordinateSystemWktReader.Parse(
            "PROJCS[\"DHDN / 3-degree Gauss zone 4\"," +
                "GEOGCS[\"DHDN\",DATUM[\"Deutsches_Hauptdreiecksnetz\"," +
                "SPHEROID[\"Bessel 1841\",6377397.155,299.1528128," +
                "AUTHORITY[\"EPSG\",\"7004\"]],AUTHORITY[\"EPSG\",\"6314\"]]," +
                "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]]," +
                "UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4314\"]]," +
                "PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",12],PARAMETER[\"scale_factor\",1],PARAMETER[\"false_easting\",4500000],PARAMETER[\"false_northing\",0]," +
                "UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AUTHORITY[\"EPSG\",\"31464\"]]") as ICoordinateSystem;

        public DhdnGkZone4Projection()
        {
            transformation = this.factory.CreateFromCoordinateSystems(csDhdnZone4, csWgs84Sphere);
        }  
    }

    public class ETRS89Projection : Projection
    {
        // ETRS89 / UTM zone 32N
        private static ICoordinateSystem csEtrs89Zone32 = SharpMap.Converters.WellKnownText.CoordinateSystemWktReader.Parse(
            "PROJCS[\"ETRS89 / UTM zone 32N\"," +
                "GEOGCS[\"ETRS89\",DATUM[\"European_Terrestrial_Reference_System_1989\"," +
                "SPHEROID[\"GRS 1980\",6378137,298.257222101," +
                "AUTHORITY[\"EPSG\",\"7019\"]],AUTHORITY[\"EPSG\",\"6258\"]]," +
                "PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]]," +
                "UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4258\"]]," +
                "PROJECTION[\"Transverse_Mercator\"],PARAMETER[\"latitude_of_origin\",0],PARAMETER[\"central_meridian\",9],PARAMETER[\"scale_factor\",0.9996],PARAMETER[\"false_easting\",500000],PARAMETER[\"false_northing\",0]," +
                "UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],AUTHORITY[\"EPSG\",\"25832\"]]") as ICoordinateSystem;

        // ETRS89 / UTM zone 33N
        private static ICoordinateSystem csEtrs89Zone33 = SharpMap.Converters.WellKnownText.CoordinateSystemWktReader.Parse(
           "PROJCS[\"ETRS89 / UTM zone 33N\"," +
           "GEOGCS[\"ETRS89\", DATUM[\"European_Terrestrial_Reference_System_1989\"," +
           "SPHEROID[\"GRS 1980\", 6378137, 298.257222101," +
           "AUTHORITY[\"EPSG\", \"7019\"]], AUTHORITY[\"EPSG\", \"6258\"]]," +
           "PRIMEM[\"Greenwich\", 0, AUTHORITY[\"EPSG\", \"8901\"]]," +
           "UNIT[\"degree\", 0.01745329251994328, AUTHORITY[\"EPSG\", \"9122\"]], AUTHORITY[\"EPSG\", \"4258\"]]," +
           "PROJECTION[\"Transverse_Mercator\"], PARAMETER[\"latitude_of_origin\", 0], PARAMETER[\"central_meridian\", 15], PARAMETER[\"scale_factor\", 0.9996], PARAMETER[\"false_easting\", 500000], PARAMETER[\"false_northing\", 0]," +
           "UNIT[\"metre\", 1, AUTHORITY[\"EPSG\", \"9001\"]], AUTHORITY[\"EPSG\", \"25833\"]]") as ICoordinateSystem;

        private ICoordinateTransformation zone32Transformation;
        private ICoordinateTransformation zone33Transformation;

        public ETRS89Projection()
        {
            zone32Transformation = this.factory.CreateFromCoordinateSystems(csEtrs89Zone32, csWgs84Sphere);
            zone33Transformation = this.factory.CreateFromCoordinateSystems(csEtrs89Zone33, csWgs84Sphere);
        }

        public override string getCoordinatesAsWGS84(int east, int north)
        {
            int zone = Int32.Parse(east.ToString().Substring(0, 2));
            east = Int32.Parse(east.ToString().Substring(2, 6));

            if (zone == 32) return this.Transform(east, north, zone32Transformation);
            else if (zone == 33) return this.Transform(east, north, zone33Transformation);
            else return null;
        }
    }
}
