﻿using System;
using System.Collections;
using M4DBO;

namespace PoSo
{
    /// <summary>
    /// PoSo Tools, contains some useful helpers.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Create a list of point-source ids from a collective.
        /// </summary>
        /// <param name="poso">PoSo database connection</param>
        /// <param name="nameOfCollective">Name of the collective, needs to exist.</param>
        /// <returns>A list of ids for all point sources contained in the named collective.</returns>
        public static dboList CreatePointSourceList(Database poso, String nameOfCollective)
        {
            dboMDCollectives collectives = poso.db.CreateObject_MDCollectives(nameOfCollective);
            IEnumerator enumerator = collectives.GetEnumerator();
            enumerator.MoveNext();

            if (enumerator.Current == null) throw new Exception("No such collective!");

            dboMDCollective collective = enumerator.Current as dboMDCollective;
            return collective.DbExecuteQuery(collective.QuerySettingsGet());
        }
    }
}
