﻿using System;
using System.Collections;
using M4DBO;

namespace PoSo
{
    /// <summary>
    /// Class wraps a point source master data set and offers access to its values.
    /// Fields marked "buffered" are only queried once and never change afterwards.
    /// You will need to create a new object to access updated information.
    /// </summary>
    public class PointSource
    {
        // Constants in database
        // Dimensions
        private static int POINTSOURCE_DIMENSION = 8;
        private static int POLLUTANT_DIMENSION = 9;
        private static int VALUETYPE_DIMENSION = 12;
        private static int LOCATION_DIMENSION = 13;
        
        // Descriptors
        private static int SULPHUROXIDE_ID = 1366;
        private static int NITROGENOXIDE_ID = 1367;
        private static int TSP_ID = 1384;
        private static int THERMAL_OUTPUT_ID = 1365;

        // Master data fields and components
        private static int FAMILY = 3;

        private static int NAMING_COMPONENT = 46;
        private static int OPERATOR_FIELD = 201;

        private static int REFERENCE_COMPONENT = 57;
        private static int PRTR_FIELD = 228;
        private static int ETS_FIELD = 213;

        private static int LOCATION_COMPONENT = 45;
        private static int LOCATION_FIELD = 140;
        private static int STATE_FIELD = 141;
        private static int COMMUNITY_CODE_FIELD = 142;
        private static int EAST_FIELD = 143;
        private static int NORTH_FIELD = 144;
        private static int GEOREF_SYS_FIELD = 149;

        private static int TECHNIQUE_COMPONENT = 47;
        private static int TECHNIQUE_FIELD = 202;

        private static int OTHER_COMPONENT = 57;
        private static int PICTURE_FIELD = 198;
        private static int REGISTER_FIELD = 213;

        // My database to work on
        protected dboDatabase db;
        // My key as object of the descriptor in the database
        // master data set is linked to
        protected dboTreeObject keyDescriptor;
        // Link to the actual data set (as annex set links)
        protected dboAnnexSetLinks annexSetLinks;

        // Local field for values, prevents database being queried multiple times
        private int prtrId = 0;
        private int etsId;
        private string psOperator;
        private string location;
        private string communityCode;
        private string pictureURL;
        private string registerURL;
        private string coordinates;
        private string technique;
        private double thermalOutput = -1;

        /// <summary>
        /// Create new point source.
        /// </summary>
        /// <param name="database">Handle to database to read from</param>
        /// <param name="key">Key of point source to represent</param>
        public PointSource(dboDatabase database, string key) : this (database, Int32.Parse(key))
        {}

        /// <summary>
        /// Create new point source.
        /// </summary>
        /// <param name="database">Handle to database to read from</param>
        /// <param name="key">Key of point source to represent</param>
        public PointSource(dboDatabase database, int key)
        {
            this.db = database;
            this.keyDescriptor = db.TreeObjects[key, POINTSOURCE_DIMENSION];

            // Assure descriptor exists
            if (this.keyDescriptor == null) throw new NullReferenceException();
            
            // Prepare annex set links for later usage
            dboList list = new dboList();
            list.Add(keyDescriptor.ObjNr); //.ToString(), VBA.VbVarType.vbInteger);

            dboAnnexObjects objects = db.CreateObject_AnnexObjects("");
            objects.DbReadByReference_MD(FAMILY, list, true);
            dboAnnexObject annexObject = objects.GetObject_MD(FAMILY, list);

            this.annexSetLinks = db.CreateObject_AnnexSetLinks("");
            this.annexSetLinks.DbReadByReference(annexObject.AnnexObjNr, mspAnnexTypeEnum.mspAnnexTypeMD, true);

            // Assure data exists
            if (this.annexSetLinks.Count < 1) throw new NullReferenceException();
        }

        /// <summary>
        /// Get the point source identifier.
        /// </summary>
        /// <returns>The identifier as defined by key given on construction</returns>
        public string GetId()
        {
            return keyDescriptor.ID;
        }

        /// <summary>
        /// Get the point source name.
        /// </summary>
        /// <returns>The name as defined by key given on construction</returns>
        public string GetName()
        {
            return keyDescriptor.Name;
        }

        /// <summary>
        /// Get the PRTR ID for this point source. (Buffered.)
        /// </summary>
        /// <returns>The ID for this point source in PRTR, or -1 if not available.</returns>
        public int GetPRTRId()
        {
            if (prtrId == 0)
            {
                dboAnnexItemData data = ExtractAnnexItemData(REFERENCE_COMPONENT, PRTR_FIELD);
                string id = data.TextData;

                if (!Int32.TryParse(id, out prtrId)) prtrId = -1;
            }

            return prtrId;
        }

        /// <summary>
        /// Get the ETS ID for this point source. (Buffered.)
        /// </summary>
        /// <returns>The ID for this point source in the ETS, or -1 if not available.</returns>
        public int GetETSId()
        {
            if (etsId == 0)
            {
                dboAnnexItemData data = ExtractAnnexItemData(REFERENCE_COMPONENT, ETS_FIELD);
                string id = data.TextData;

                if (id.Contains("=")) id = id.Split('=')[1];

                if (!Int32.TryParse(id, out etsId)) etsId = -1;
            }

            return etsId;
        }

        /// <summary>
        /// Get the operating company for this point source. (Buffered.)
        /// </summary>
        /// <returns>The name of the operator.</returns>
        public String GetOperator()
        {
            if (psOperator == null)
            {
                dboAnnexItemData data = ExtractAnnexItemData(NAMING_COMPONENT, OPERATOR_FIELD);
                if (data != null) psOperator = data.TextData;
            }

            return psOperator;
        }

        /// <summary>
        /// Get the name of the location of point source represented. (Buffered.)
        /// </summary>
        /// <returns>The name of the location, e.g.: "New York"</returns>
        public string GetLocation()
        {
            if (location == null)
            {
                dboAnnexItemData data = ExtractAnnexItemData(LOCATION_COMPONENT, LOCATION_FIELD);
                location = db.TreeObjects[data.ReferenceData, LOCATION_DIMENSION].Name;
            }

            return location;
        }

        /// <summary>
        /// Get the German "Land" for this point source.
        /// </summary>
        /// <returns>The state of the point source, e.g.: "Hessen"</returns>
        public State GetState()
        {
            dboAnnexItemData data = ExtractAnnexItemData(LOCATION_COMPONENT, STATE_FIELD);
            foreach(State state in Enum.GetValues(typeof(State)))
                if (state.LongName().Equals(db.TreeObjects[data.ReferenceData, LOCATION_DIMENSION].Name.ToString())) return state;
            
            throw new ArgumentOutOfRangeException("state", "No such state");
        }

        /// <summary>
        /// Get the community code for this point source. (Buffered.)
        /// </summary>
        /// <returns>The code as saved in the data base</returns>
        public string GetCommunityCode()
        {
            if (communityCode == null)
            {
                dboAnnexItemData data = ExtractAnnexItemData(LOCATION_COMPONENT, COMMUNITY_CODE_FIELD);
                communityCode = data.TextData;
            }

            return communityCode;
        }

        /// <summary>
        /// Get the coordinates for this point source. (Buffered.)
        /// Will try any available method to return some plausible coordinates in WGS84 projection
        /// as used by Google Maps/Earth. Note that this is error-prone though.
        /// </summary>
        /// <returns>Coordinates as "[long],[lat]" in WGS84 projection (separated by comma, e.g.: "3.4923454,45.2321311"),
        ///     or [null] for no location found</returns>
        public string GetCoordinates()
        {
            return GetCoordinates(true);
        }

        /// <summary>
        /// Get the coordinates for this point source. (Buffered.)
        /// Note that this is error-prone though.
        /// </summary>
        /// <param name="fallbackToGeoLocate">If true, method tries to use Google's geo-coding service
        /// if no coordinates are given for this point source</param>
        /// <returns>Coordinates as "[long],[lat]" in WGS84 projection (separated by comma, e.g.: "3.4923454,45.2321311"),
        ///     or [null] for no location found</returns>
        public string GetCoordinates(bool fallbackToGeoLocate)
        {
            if (coordinates == null)
            {
                dboAnnexItemData eastItemData = ExtractAnnexItemData(LOCATION_COMPONENT, EAST_FIELD);
                dboAnnexItemData northItemData = ExtractAnnexItemData(LOCATION_COMPONENT, NORTH_FIELD);
                dboAnnexItemData georefSysItemData = ExtractAnnexItemData(LOCATION_COMPONENT, GEOREF_SYS_FIELD);

                int east = 0;
                int north = 0;
                if (eastItemData != null && Int32.TryParse(eastItemData.NumberData.ToString(), out east) &&
                    northItemData != null && Int32.TryParse(northItemData.NumberData.ToString(), out north) &&
                    georefSysItemData != null && georefSysItemData.ReferenceData > 0)
                {
                    Projection projection = GetProjectionForTextPoolID(georefSysItemData.ReferenceData);
                    coordinates = projection.getCoordinatesAsWGS84(east, north);
                }
                else if (fallbackToGeoLocate)
                {
                    // The translation above failed -> fallback to geo coding if enabled
                    coordinates = GetCoordinatesFromGeoLocation();
                }
            }

            return coordinates;
        }

        /// <summary>
        /// Get the coordinates for this point source from geo location.
        /// Do not use unless no other coordinates are available.
        /// </summary>
        /// <returns>(Possible) coordinates for this point source.</returns>
        public String GetCoordinatesFromGeoLocation()
        {
            dboAnnexItemData data = ExtractAnnexItemData(LOCATION_COMPONENT, LOCATION_FIELD);
            String location = db.TreeObjects[data.ReferenceData, LOCATION_DIMENSION].ID;

            return GeoHelper.GeoLocate(location + ", " + GetState().LongName());
        }

        /// <summary>
        /// Get the technique information for this point source. (Buffered.)
        /// </summary>
        /// <returns>The name of the technique.</returns>
        public String GetTechnique()
        {
            if (technique == null)
            {
                dboAnnexItemData data = ExtractAnnexItemData(TECHNIQUE_COMPONENT, TECHNIQUE_FIELD);

                dboAnnexItemTextPools pools = db.CreateObject_AnnexItemTextPools(data.ReferenceData.ToString());
                pools.DbReadAll();
                
                technique = pools[data.ReferenceData].TextData;
            }

            return technique;
        }

        /// <summary>
        /// Get the internet resource link to the Emission Trading System (ETS) account
        /// of this point source (Buffered.)
        /// </summary>
        /// <returns>Complete URL to picture or null if none available</returns>
        public string GetETSRegisterURL()
        {
            if (registerURL == null)
            {
                dboAnnexItemData data = ExtractAnnexItemData(OTHER_COMPONENT, REGISTER_FIELD);
                if (data != null && data.TextData.StartsWith("http"))
                    registerURL = data.TextData;
            }

            return registerURL;
        }

        /// <summary>
        /// Get a link to a picture of the point source. (Buffered.)
        /// </summary>
        /// <returns>Complete URL to picture or null if none available</returns>
        public string GetPictureURL()
        {
            if (pictureURL == null)
            {
                dboAnnexItemData data = ExtractAnnexItemData(OTHER_COMPONENT, PICTURE_FIELD);
                if (data != null) 
                    pictureURL = data.TextData;
            }

            return pictureURL;
        }

        /// <summary>
        /// Get the thermal output of this point source. (Buffered.)
        /// </summary>
        /// <param name="year">The year to look for, method will attempt to find earlier
        ///     values if no current is available</param>
        /// <returns>The thermal output in mega watts or -1 for no information</returns>
        public double GetThermalOutput(DateTime year)
        {
            if (thermalOutput < 0)
            {
                dboTS serie = GetFirstTimeSeries(VALUETYPE_DIMENSION, THERMAL_OUTPUT_ID);
                if (serie != null)
                {
                    ReadData(serie, year.AddYears(-3), year);

                    dboTSData thermalOutputData = null;
                    double value;
                    foreach (dboTSData data in serie.TSDatas)
                            // First value found
                        if ((thermalOutputData == null && data.Value != null) ||
                            // Better, new value found
                            (data.Value != null && data.PeriodNr > thermalOutputData.PeriodNr) &&
                            // Use only if actual value
                            Double.TryParse(data.Value.ToString(), out value))
                            thermalOutputData = data;

                    if (thermalOutputData != null)
                        Double.TryParse(thermalOutputData.Value.ToString(), out thermalOutput);
                }
            }

            return thermalOutput;
        }

        /// <summary>
        /// Get the sulphur oxide (SOx) emission of this point source (in tons)
        /// </summary>
        /// <param name="fromYear">Year returned time series starts</param>
        /// <param name="toYear">Year returned time series ends</param>
        /// <returns>An array of emission values in kilo grams and rounded, ordered latest last or
        ///     null if toYear before fromYear. Values may be zero for all or some years.</returns>
        public double[] GetSulphurOxideEmission(DateTime fromYear, DateTime toYear)
        {
            return GetEmission(SULPHUROXIDE_ID, fromYear, toYear);
        }

        /// <summary>
        /// Get the nitrogen oxide (NOx) emission of this point source (in tons)
        /// </summary>
        /// <param name="fromYear">Year returned time series starts</param>
        /// <param name="toYear">Year returned time series ends</param>
        /// <returns>An array of emission values in kilo grams and rounded, ordered latest last or
        ///     null if toYear before fromYear. Values may be zero for all or some years.</returns>
        public double[] GetNitrogenOxideEmission(DateTime fromYear, DateTime toYear)
        {
            return GetEmission(NITROGENOXIDE_ID, fromYear, toYear);
        }

        /// <summary>
        /// Get the total suspended particulate matter (TSP) emission of this point source (in tons)
        /// </summary>
        /// <param name="fromYear">Year returned time series starts</param>
        /// <param name="toYear">Year returned time series ends</param>
        /// <returns>An array of emission values in kilo grams and rounded, ordered latest last or
        ///     null if toYear before fromYear. Values may be zero for all or some years.</returns>
        public double[] GetTSPEmission(DateTime fromYear, DateTime toYear)
        {
            return GetEmission(TSP_ID, fromYear, toYear);
        }

        private double[] GetEmission(int pollutant, DateTime fromYear, DateTime toYear)
        {
            if (toYear.Year < fromYear.Year) return null;

            double[] result = new double[toYear.Year - fromYear.Year + 1];

            dboTS serie = GetFirstTimeSeries(POLLUTANT_DIMENSION, pollutant);
            if (serie != null)
            {
                ReadData(serie, fromYear, toYear);

                double emission = 0;

                foreach (dboTSData data in serie.TSDatas)
                    if (fromYear.Year <= data.PeriodNr + 2000 && toYear.Year >= data.PeriodNr + 2000 
                        && Double.TryParse(data.Value.ToString(), out emission))
                        result[(data.PeriodNr + 2000) - fromYear.Year] = Math.Round(emission, 0);
            }

            return result;
        }

        private Projection GetProjectionForTextPoolID(int ID)
        {
            // {DhdnGkZone2 = 168, DhdnGkZone3 = 121, DhdnGkZone4 = 195, ETRS89 = 250};
            switch (ID)
            {
                case 168: return new DhdnGkZone2Projection();
                case 121: return new DhdnGkZone3Projection();
                case 195: return new DhdnGkZone4Projection();
                case 250: return new ETRS89Projection();
            }

            return null;
        }

        private dboAnnexItemData ExtractAnnexItemData(int componentNumber, int fieldNumber)
        {
            dboAnnexItemData result = null;

            IEnumerator linksEnum = annexSetLinks.GetEnumerator();
            while (linksEnum.MoveNext())
            {
                dboAnnexSetLink link = linksEnum.Current as dboAnnexSetLink;

                if (link.ComponentNr == componentNumber)
                {
                    dboAnnexItemDatas datas = db.CreateObject_AnnexItemDatas("");
                    datas.DbReadByItemNr(link.AnnexSetNr, componentNumber, fieldNumber, false,
                        link.AnnexSetLinkNr, true);

                    IEnumerator dataEnum = datas.GetEnumerator();
                    dataEnum.MoveNext();
                    result = dataEnum.Current as dboAnnexItemData;
                    break;
                }
            }

            return result;
        }

        private dboTreeObjectFilter CreateObjectFilter(int dimensionId, int descriptorId)
        {
            dboTreeObjectFilter objectFilter = db.CreateObject_TreeObjectFilter();
            objectFilter.DimNr = dimensionId;
            objectFilter.Numbers = descriptorId.ToString();

            return objectFilter;
        }

        private dboTS GetFirstTimeSeries(int dimension, int descriptor)
        {
            dboTSFilter filter = db.CreateObject_TSFilter();
            filter.FilterUsage = mspFilterUsageEnum.mspFilterUsageFilterOnly;
            filter.Add(CreateObjectFilter(POINTSOURCE_DIMENSION, keyDescriptor.ObjNr));
            filter.Add(CreateObjectFilter(dimension, descriptor));

            dboTSs series = db.CreateObject_TSs(filter.GetTSNumbers());

            IEnumerator seriesEnumerator = series.GetEnumerator();
            seriesEnumerator.MoveNext();
            
            return seriesEnumerator.Current as dboTS;
        }

        private void ReadData(dboTS serie, DateTime fromYear, DateTime toYear)
        {
            int to = db.Units.TkDateToPeriod(new DateTime(toYear.Year, 1, 1), mspTimeKeyEnum.mspTimeKeyYear, false);
            int from = db.Units.TkDateToPeriod(new DateTime(fromYear.Year, 1, 1), mspTimeKeyEnum.mspTimeKeyYear, false);

            serie.DbReadRelatedDatas(true, mspDataTypeEnum.mspDataTypeInput,
                mspTimeKeyEnum.mspTimeKeyYear, from, to, null, "7", null);
        }
    }
}
