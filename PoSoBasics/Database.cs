﻿using System;
using M4DBO;

namespace PoSo
{
    /// <summary>
    /// Handle to database connection
    /// </summary>
    public class Database
    {
        private static String DATABASE_ID = "PoSo_aktuell";
        private dboRoot root;
        
        /// <summary>
        /// Handle to PoSo database. Do not use unless Connect() was called successfully!
        /// </summary>
        public dboDatabase db;
        
        /// <summary>
        /// Connect to PoSo database.
        /// </summary>
        /// <param name="programName">Name of program wishing to connect.</param>
        public void Connect(String programName)
        {
            Connect(true, false, programName);
        }

        /// <summary>
        /// Connect to PoSo database.
        /// </summary>
        /// <param name="readOnly">Open database read-only?</param>
        /// <param name="exclusive">Open database exclusivly?</param>
        /// <param name="programName">Name of program wishing to connect.</param>
        public void Connect(bool readOnly, bool exclusive, String programName)
        {
            mspErrDboInitEnum rootErr;
            mspErrDboOpenDbEnum databaseErr;

            root = new M4DBO.dboRoot();
            rootErr = root.Initialize("", mspM4AppEnum.mspM4AppOEM, false, programName);

            if (rootErr == mspErrDboInitEnum.mspErrNone)
            {
                // This might need to be changed to root.Login("<username>", "<pw>")
                root.LoginWithNTAccount();

                databaseErr = root.Databases.OpenDb(DATABASE_ID, readOnly, ref exclusive);
                if (databaseErr == mspErrDboOpenDbEnum.mspErrNone) db = root.MainDb;
                else throw new Exception("Connection to PoSo database failed: " + databaseErr.ToString());
            }
            else throw new Exception("Connection to PoSo database failed: " + rootErr.ToString());
        }

        /// <summary>
        /// Logout from PoSo database.
        /// </summary>
        public void Disconnect()
        {
            root.Logout();
        }
    }
}
