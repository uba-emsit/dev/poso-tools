﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PoSo;
using System.ComponentModel;

namespace PoSoPowerPlantDBConnector
{
    public partial class UserInterface : Window
    {
        GridViewColumnHeader _lastHeaderClickedPointSources = null;
        ListSortDirection _lastDirectionPointSources = ListSortDirection.Ascending;

        GridViewColumnHeader _lastHeaderClickedPowerPlants = null;
        ListSortDirection _lastDirectionPowerPlants = ListSortDirection.Ascending;
        
        public UserInterface()
        {
            InitializeComponent();

            foreach (PointSource source in ((Connector)Application.Current).pointSources)
                _PointSourceListView.Items.Add(new PointSourceWrapper(source));

            AddAllPowerPlants();
        }

        private void Search(object sender, RoutedEventArgs e)
        {
            if (_PointSourceListView.SelectedItem == null)
            {
                MessageBox.Show("Bitte wählen Sie eine Punktquelle!", "Keine Punktquelle", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            _PowerPlantListView.Items.Clear();

            PointSource pointSource = (_PointSourceListView.SelectedItem as PointSourceWrapper).PointSource;
            
            int limit = 0;

            if (_MatchByState.IsChecked.Value) limit++;
            if (_MatchByLocation.IsChecked.Value) limit++;
            if (_MatchByOperator.IsChecked.Value) limit++;

            foreach (PowerPlant plant in ((Connector)Application.Current).powerPlants)
            {
                int score = 0;
                 
                if (plant.State.Equals(pointSource.GetState()) && _MatchByState.IsChecked.Value) score++;
                if ((plant.Name.Contains(pointSource.GetLocation()) || pointSource.GetLocation().Contains(plant.Name)) &&
                    _MatchByLocation.IsChecked.Value) score++;

                if (score >= limit) _PowerPlantListView.Items.Add(plant);
            }

        }

        private void UndoSearch(object sender, RoutedEventArgs e)
        {
            AddAllPowerPlants();
        }

        private void AddAllPowerPlants()
        {
            _PowerPlantListView.Items.Clear();

            foreach (PowerPlant plant in ((Connector)Application.Current).powerPlants)
                _PowerPlantListView.Items.Add(plant);
        }

        private void Write(object sender, RoutedEventArgs e)
        {
            PointSource pointSource = (_PointSourceListView.SelectedItem as PointSourceWrapper).PointSource;
            if (pointSource == null)
            {
                MessageBox.Show("Bitte wählen Sie eine Punktquelle!", "Keine Punktquelle", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            PowerPlant plant = _PowerPlantListView.SelectedItem as PowerPlant;
            if (plant == null)
            {
                MessageBox.Show("Bitte wählen Sie ein Kraftwerk!", "Kein Kraftwerk", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            Console.WriteLine(pointSource.GetId() + ";" + plant.StartYear + ";" + plant.Operator);
        }

        private void SortPointSources(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction = ListSortDirection.Ascending;

            if (headerClicked != null)
                if (headerClicked != _lastHeaderClickedPointSources)
                    direction = ListSortDirection.Ascending;
                else
                    if (_lastDirectionPointSources == ListSortDirection.Ascending)
                        direction = ListSortDirection.Descending;
                    else
                        direction = ListSortDirection.Ascending;


            string header = headerClicked.Column.Header as string;
            _PointSourceListView.Items.SortDescriptions.Clear();
            SortDescription sortDescription = new SortDescription(PointSourcePropertyForHeader(header), direction);
            _PointSourceListView.Items.SortDescriptions.Add(sortDescription);
            _PointSourceListView.Items.Refresh();
            
            _lastHeaderClickedPointSources = headerClicked;
            _lastDirectionPointSources = direction;
        }

        private string PointSourcePropertyForHeader(string header)
        {
            if (header.Equals("ID")) return "Id";
            else if (header.Equals("Ort")) return "Location";
            else if (header.Equals("Land")) return "State";
            else if (header.Equals("Betreiber")) return "Operator";
            else if (header.Equals("Technik")) return "Technique";
            else return header;
        }

        private void SortPowerPlants(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction = ListSortDirection.Ascending;

            if (headerClicked != null)
                if (headerClicked != _lastHeaderClickedPowerPlants)
                    direction = ListSortDirection.Ascending;
                else
                    if (_lastDirectionPowerPlants == ListSortDirection.Ascending)
                        direction = ListSortDirection.Descending;
                    else
                        direction = ListSortDirection.Ascending;


            string header = headerClicked.Column.Header as string;
            _PowerPlantListView.Items.SortDescriptions.Clear();
            SortDescription sortDescription = new SortDescription(PowerPlantPropertyForHeader(header), direction);
            _PowerPlantListView.Items.SortDescriptions.Add(sortDescription);
            _PowerPlantListView.Items.Refresh();

            _lastHeaderClickedPowerPlants = headerClicked;
            _lastDirectionPowerPlants = direction;
        }

        private string PowerPlantPropertyForHeader(string header)
        {
            if (header.Equals("ID")) return "Id";
            else if (header.Equals("Betriebsaufnahme")) return "StartYear";
            else if (header.Equals("Land")) return "StateName";
            else if (header.Equals("Betreiber")) return "Operator";
            else if (header.Equals("Technik")) return "Technique";
            else if (header.Equals("Ertüchtigung")) return "Renovation";
            else return header;
        }
    }

    class PointSourceWrapper
    {
        private PointSource pointSource;

        public PointSourceWrapper(PointSource source)
        {
            this.pointSource = source;
        }

        internal PointSource PointSource
        {
            get
            {
                return pointSource;
            }
        }

        public String Id
        {
            get
            {
                return pointSource.GetId();
            }
        }

        public String Name
        {
            get
            {
                return pointSource.GetName();
            }
        }

        public String Location
        {
            get
            {
                return pointSource.GetLocation();
            }
        }

        public String State
        {
            get
            {
                return pointSource.GetState().LongName();
            }
        }

        public String Operator
        {
            get
            {
                return pointSource.GetOperator();
            }
        }

        public String Technique
        {
            get
            {
                return pointSource.GetTechnique();
            }
        }
    }
}
