﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoSoFuelMigration
{
    class Program
    {
        static string WORK_FOLDER = "C:\\Users\\hausmann\\Entwicklung\\Visual Studio Workspace\\Projects\\PoSoFuelMigration\\PoSoFuelMigration\\";
        static string DATA_FILE = WORK_FOLDER + "data.csv";
        static string FUEL_FILE = WORK_FOLDER + "fuels.csv";
        static string INSERT_SCRIPT = WORK_FOLDER + "script.sql";
        static string UNKNOWN_FUELS_LOG = WORK_FOLDER + "unknown.log";

        static char DELIMITER = ';';
        static string NO_DATA = "Kein Stammdatenwert";

        static string SCRIPT_PREFIX = "SET DATEFORMAT ymd;" + Environment.NewLine;
        static string INSERT_TEMPLATE = "INSERT INTO [dbo].[AnnexItemData] ([ItemDataNr], [AnnexSetNr], [AnnexType], [AnnexSetLinkNr], [ItemNr], [ItemType], [ComponentNr], [TextData], [NumberData], [ReferenceData], [CalendarData], [FileName], [ValidFromDate], [ValidToDate], [ChangeID], [ChangeDate], [Origin], [IsHistory], [IsPoolObj], [IsDeleted])" + Environment.NewLine
            + "VALUES ({0}, {1}, 1, {1}, 330, 16, 47, N'', NULL, {2}, N'1899-12-30 00:00:00', N'', N'{3}-01-01 00:00:00', N'{4}-12-31 00:00:00', N'Kludt_Admin', N'2017-10-17 11:10:35', 3, {5}, 0, 0);" + Environment.NewLine;

        static void Main(string[] args)
        {
            Console.WriteLine("Okay, getting started...");

            Console.WriteLine("Reading fuel map...");

            Dictionary<string, int> fuels = new Dictionary<string, int>();
            foreach (string fuel in File.ReadAllLines(FUEL_FILE).Skip(1))
                fuels.Add(fuel.Split(DELIMITER)[1], Int32.Parse(fuel.Split(DELIMITER)[0]));

            Console.WriteLine("Found " + fuels.Count + " fuels...");

            Console.WriteLine("Reading data...");

            int index = 1000000;
            File.WriteAllText(INSERT_SCRIPT, SCRIPT_PREFIX);
            File.Delete(UNKNOWN_FUELS_LOG);
            foreach (string plant in File.ReadAllLines(DATA_FILE).Skip(3))
            {
                string[] details = plant.Split(DELIMITER);
                string annexSetNr = details[0];
                Console.WriteLine("Reading: " + details[3]);

                int indexHelper = index;
                bool isFirstEntry = true;

                for (int year = 2014; year >= 2006; year--)
                {
                    int startindex = 4 + (2014 - year) * 9;

                    for (int fuelindex = 0; fuelindex < 8; fuelindex = fuelindex + 2)
                        if (!details[startindex + fuelindex].Equals(NO_DATA))
                        {
                            string valueString = details[startindex + fuelindex + 1];
                            bool isValue2 = !valueString.Equals(NO_DATA) && valueString.Split(' ').Length > 1 && valueString.EndsWith(")");
                            double value = !isValue2 ? -1 : Double.Parse(valueString.Split(' ')[0]);
                            int valueYear = !isValue2 ? -1 : Int32.Parse(valueString.Substring(valueString.Length - 5, 4));

                            if (fuels.ContainsKey(details[startindex + fuelindex]) && value > 0 && valueYear == year)
                                // ItemDataNr, AnnexSetNr = AnnexSetLinkNr, ReferenceData, ValidFromDate, ValidToDate, IsHistory
                                File.AppendAllText(INSERT_SCRIPT, String.Format(INSERT_TEMPLATE, index++, annexSetNr, fuels[details[startindex + fuelindex]], year == 2006 ? "1900" : year.ToString(), isFirstEntry ? "9999" : year.ToString(), isFirstEntry ? "0" : "-1"));
                            else if (details[startindex + fuelindex].Equals("Sonstige (siehe Bemerkung)") && value > 0 && valueYear == year)
                            {
                                string fuel = "";
                                switch (fuelindex)
                                {
                                    case 0: fuel = "7321"; break; // Fest
                                    case 2: fuel = "7318"; break; // Flüssig
                                    case 4: fuel = "7317"; break; // Bio
                                    case 6: fuel = "7322"; break; // Gas
                                    default:
                                        break;
                                }
                                // ItemDataNr, AnnexSetNr = AnnexSetLinkNr, ReferenceData, ValidFromDate, ValidToDate, IsHistory
                                File.AppendAllText(INSERT_SCRIPT, String.Format(INSERT_TEMPLATE, index++, annexSetNr, fuel, year == 2006 ? "1900" : year.ToString(), isFirstEntry ? "9999" : year.ToString(), isFirstEntry ? "0" : "-1"));
                            }
                            else if (value > 0 && valueYear == year)
                                File.AppendAllText(UNKNOWN_FUELS_LOG, details[0] + DELIMITER + details[3] + DELIMITER + details[startindex + fuelindex] + Environment.NewLine);
                        }

                    // Special case for natural gas
                    string gasString = details[startindex + 8];
                    bool isValue = !gasString.Equals(NO_DATA) && gasString.Split(' ').Length > 1 && gasString.EndsWith(")");
                    double gasValue = !isValue ? -1 : Double.Parse(gasString.Split(' ')[0]);
                    int gasYear = !isValue ? -1 : Int32.Parse(gasString.Substring(gasString.Length - 5, 4));
                    if (!gasString.Equals(NO_DATA) && gasValue > 0 && gasYear == year)
                        File.AppendAllText(INSERT_SCRIPT, String.Format(INSERT_TEMPLATE, index++, annexSetNr, fuels["Erdgas"], year == 2006 ? "1900" : year.ToString(), isFirstEntry ? "9999" : year.ToString(), isFirstEntry ? "0" : "-1"));

                    isFirstEntry = indexHelper == index;
                }                
            }

            Console.WriteLine("Done! Check log for unknown fuels!");
            Console.ReadKey();
        }
    }
}
