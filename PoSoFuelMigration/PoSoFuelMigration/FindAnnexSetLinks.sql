﻿SELECT TreeObject.ObjNr, TreeObject.Id, TreeObject.Name, MDKey.AnnexObjNr, MDKey.AnnexSetNr, MDKey.AnnexSetLinkNr, MDKey.ComponentNr
FROM TreeObject
INNER JOIN (
	SELECT MDKey.AnnexObjNr, MDKey.ObjNr, AnnexSetLink.AnnexSetNr, AnnexSetLink.AnnexSetLinkNr, AnnexSetLink.ComponentNr
	FROM MDKey
	INNER JOIN (
		SELECT AnnexSetLink.AnnexObjNr, AnnexSetLink.AnnexSetNr, AnnexSetLink.AnnexSetLinkNr, AnnexSetLink.ComponentNr
		FROM AnnexSetLink) AnnexSetLink
	ON MDKey.AnnexObjNr = AnnexSetLink.AnnexObjNr) MDKey
ON TreeObject.ObjNr = MDKey.ObjNr
where TreeObject.DimNr = 8 and ComponentNr = 47